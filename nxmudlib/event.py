import nxmudlib.util

iota = nxmudlib.util.make_iota()

EVENT_JOIN = iota()
EVENT_LEAVE = iota()
EVENT_INFO = iota()
EVENT_WARN = iota()
EVENT_ERROR = iota()

def _string_to_kind(s):
    ls = s.lower()
    #HACK
    if ls == "join":
        return EVENT_JOIN
    elif ls == "leave":
        return EVENT_LEAVE
    elif ls == "info":
        return EVENT_INFO
    elif ls == "warn":
        return EVENT_WARN
    elif ls == "error":
        return EVENT_ERROR
    else:
        return 0


class Event():
    def __init__(self, data):
        self.kind_string = data[0]
        self.kind = _string_to_kind(self.kind_string)
        self.args = data[:1]
