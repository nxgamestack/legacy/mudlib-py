import socket
from nxmudlib.telnetconnection import TelnetConnection

class TCPClient():
    def __init__(self, hostname="localhost", port=1234):
        #self.hostname = "localhost"
        self.hostname = hostname
        #self.port = 1234
        self.port = port
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connection = TelnetConnection(self._sock, -1)
        #self.events = []
        self.messages = []
        self.__connect()

    def __connect(self):
        self._sock.connect((self.hostname, self.port))
        self._sock.setblocking(False)

    #def __push_event(self, e):
    #    self.events.insert(0, e)

    #def pop_event(self):
    #    try:
    #        return self.events.pop()
    #    except IndexError:
    #        return None

    def pop_message(self):
        return self.connection.pop_message()

    def send_string(self, s):
        self.connection.send_string(s)

    def tick(self):
        self.connection.receive()
