def make_iota(start = 1):
    t = {}
    t["n"] = start - 1
    def iota():
        t["n"] += 1
        return t["n"]
    return iota
