import nxmudlib.tcpserver
import nxmudlib.player
import nxmudlib.message
import nxmudlib.event

class MUDServer():
    def __init__(self, hostname="0.0.0.0", port=1234):
        self.server = nxmudlib.tcpserver.TCPServer(hostname, port)
        self.players = {}
        self.nameprompt = "what mayest be thy name ?"
        self.motd = ""
        #self.name_whitelist = ["white", "black"]
        self.name_whitelist = []
        self.messages = []
        self.events = []

        self.whitelist_public = True

    def __push_event(self, e):
        #self.events.append(e)
        self.events.insert(0, e)

    def pop_event(self) -> nxmudlib.event.Event:
        try:
            return self.events.pop()
        except IndexError:
            return None

    def __push_message(self, message):
        self.messages.insert(0, message)

    def pop_message(self) -> nxmudlib.message.Message:
        try:
            return self.messages.pop()
        except IndexError as e:
            return None

    def send_string(self, id, s):
        self.server.send_string(id, s)

    def broadcast_string(self, s):
        for pid, p in self.players.items():
            self.server.send_string(pid, s)

    def id_to_name(self, pid):
        try:
            return self.players[pid].name
        except KeyError as e:
            return None

    def name_to_id(self, name):
        for pid, p in self.players.items():
            #print("{} vs {}".format(p.name, name))
            if p.name == name:
                return pid
        return None

    def __handle_events(self):
        e = self.server.pop_event()
        while e:
            if e[0] == "join":
                #print("{} joined ".format(e[1]))
                #server.send_string(e[1], "what mayest be thy name ?")
                self.players[e[1]] = nxmudlib.player.Player()

            if e[0] == "leave":
                #print("{} left ".format(e[1]))
                try:
                    del(self.players[e[1]])
                except KeyError as e:
                    print("<!> player that hasn't joined left the game >{}<".format(e[1]))

            self.__push_event(nxmudlib.event.Event(e))
            e = self.server.pop_event()

    def __handle_new_players(self):
        for pid, p in self.players.items():
            if p.curstate == nxmudlib.player.STATE_FLOATING:
                self.server.send_string(pid, self.nameprompt)
                p.curstate = nxmudlib.player.STATE_WAIT_NAME

    def __handle_messages(self):
        msg  = self.server.pop_message()
        while msg:
            pid = msg[0]
            segs = msg[1].strip().split(" ")
            if self.players[pid].curstate == nxmudlib.player.STATE_WAIT_NAME:
                #desired_name = msg[1]
                desired_name = segs[0]
                if desired_name and '\x00' not in desired_name:
                    if self.name_to_id(desired_name):
                        self.server.send_string(pid, "\"{}\" already taken".format(desired_name))
                    elif self.name_whitelist and not desired_name in self.name_whitelist:
                        if self.whitelist_public:
                            self.server.send_string(pid, "\"{}\" not in whitelist choose from: {}".format(desired_name, ", ".join(self.name_whitelist)))
                        else:
                            self.server.send_string(pid, "\"{}\" not in whitelist")
                    else:
                        self.players[pid].name = desired_name
                        self.players[pid].curstate = nxmudlib.player.STATE_PLAYER
                        self.server.send_string(pid, "Welcome {}".format(desired_name))
                        if self.motd:
                            self.server.send_string(pid, self.motd)
                else:
                    self.server.send_string(pid, "invalid name")
            else:
                self.__push_message(nxmudlib.message.Message(msg, self.players[pid].name))


            msg  = self.server.pop_message()

    def tick(self):
        self.server.tick()
        self.__handle_events()
        self.__handle_new_players()
        self.__handle_messages()
