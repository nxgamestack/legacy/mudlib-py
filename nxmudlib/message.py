class Message():
    def __init__(self, msg, name=""):
        #self.data = msg
        self._player_id  = msg[0]
        self._raw_message = msg[1]
        self.segs = msg[1].strip().split(" ")
        self._player_name = name
        self._command = self.segs[0]
        if len(self.segs) > 1:
            self._arguments = self.segs[1:]
        else:
            self._arguments = []

        #self.segs = msg
        #self._pid  = self.segs[0]

        #self._head = self.segs[1]
        #self._data = self.segs[1:]
        #if len(self.segs) > 2:
        #    self._tail = self.segs[2:]
        #else:
        #    self._tail =  []

        #self._pname = name




    @property
    def player_name(self):
        return self._player_name

    @property
    def player_id(self):
        return self._player_id

    @property
    def command(self):
        return self._command

    @property
    def arguments(self):
        return self._arguments

    @property
    def raw_message(self):
        return self._raw_message
