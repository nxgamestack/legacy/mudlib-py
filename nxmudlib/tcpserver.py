#import selectors
import socket
import select
#import string
#from nxnet import grace
from nxmudlib.telnetconnection import TelnetConnection

class TCPServer:
    def __init__(self, hostname="0.0.0.0", port=1234):
        self.hostname = hostname
        self.port = port
        self._tsize = 4096

        #self.connection_class = Connection
        self.connection_class = TelnetConnection

        self._connections = {}
        # use some kind id ID pool instead
        self._curid = 1 # start at 1 because 0 is falsy
        self.events = []
        self.messages = []

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.__connect()

    def __push_event(self, e):
        self.events.insert(0, e)

    def pop_event(self):
        try:
            return self.events.pop()
        except IndexError:
            return None

    def __push_message(self, msg):
        self.messages.insert(0, msg)

    def pop_message(self):
        try:
            return self.messages.pop()
        except IndexError:
            return None

    def __connect(self):
        self._sock.bind((self.hostname, self.port))
        self._sock.setblocking(False)
        self._sock.listen(8)

    def __add_connection(self, sock):
        sock.setblocking(False)
        #tmp_connection = Connection(sock, self._curid)
        tmp_connection = self.connection_class(sock, self._curid)
        self._connections[self._curid] = tmp_connection
        self.__push_event(["join", self._curid])
        self._curid += 1

    def __drop_connection(self, id):
        del(self._connections[id])
        self.__push_event(["leave", id])

    def __accept(self):
        rfiles, wfiles, xfiles = select.select([self._sock], [], [], 0)
        if rfiles:
            new_sock, addr = self._sock.accept()
            self.__add_connection(new_sock)

    def __read_all(self):
        for id, conn in list(self._connections.items()):
            conn.receive()
            tmp_msg = conn.pop_message()
            while tmp_msg:
                if tmp_msg:
                    self.__push_message([id, tmp_msg])
                tmp_msg = conn.pop_message()
            if not conn.alive:
                self.__drop_connection(id)

    def send_string(self, id, s):
        try:
            conn = self._connections[id]
            conn.send_string(s)
        except socket.error:
            #print("FIXME HANDLE SEND ERRORS")
            self.__drop_connection(id)
        except KeyError:
            print("<!> INVALID SEND TARGET")


    def tick(self):
        self.__accept()
        self.__read_all()
