
class CommandHandler():
    def __init__(self):
        self.commands = {}

    def add_command(self, name, callback):
        self.commands[name] = callback

    def do(self, commandName, args=[], err_arg_count=None, err_default=None):
        try:
            return self.commands[commandName](*args)
        except TypeError as e:
            # wrong argument count
            return err_arg_count
        except Exception as e:
            # other error
            pass
        return err_default
