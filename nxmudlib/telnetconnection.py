import socket
import select

READ_STATE_TEXT = 1
READ_STATE_CMD = 2
READ_STATE_SNEG = 3

CMD_SE   = 240 # Subnegotiation End
CMD_NOP  = 241 # No OPeration
CMD_DM   = 242 # Data Mark
CMD_BRK  = 243 # BReaK
CMD_IP   = 244 # Interrupt Process
CMD_AO   = 245 # Abort Output
CMD_AYT  = 246 # Are You There ?
CMD_EC   = 247 # Erase Character
CMD_EL   = 248 # Erase Line
CMD_GA   = 249 # Go Ahead
CMD_SB   = 250 # Subnegotiation Begin
CMD_WILL = 251 # i WILL do that, okay ?
CMD_WONT = 252 # i WON'T do that, okay ?
CMD_DO   = 253 # DO that, please
CMD_DONT = 254 # DON'T do that, please
CMD_IAC  = 255 # Interpret As Command


class TelnetConnection:
    def __init__(self, sock, id):
        self.sock = sock
        self.id = id

        #self.encoding = "latin1"
        self.encoding = "utf8"
        #self.encoding = "ISO-8859-1"
        self._tsize = 4096

        self.alive = True
        self.read_state = READ_STATE_TEXT
        self.text_buffer = bytearray()
        self.cmd_buffer = bytearray()
        self.se_buffer = bytearray()
        self.messages = []
        self.commands = []
        self.subnegotiations = []

    def __push_message(self, msg):
        self.messages.insert(0, msg)

    def pop_message(self):
        try:
            return self.messages.pop()
        except IndexError:
            return None

    def __push_command(self, cmd):
        self.commands.insert(0, cmd)

    def pop_command(self):
        try:
            return self.commands.pop()
        except IndexError:
            return None

    def __push_subnegotiation(self, sn):
        self.subnegotiations.insert(0, sn)

    def pop_subnegotiation(self):
        try:
            return self.subnegotiations.pop()
        except IndexError:
            return None

    def parse(self, data):
        #message = ""

        for c in data:
            #print("{} / {} / {} / {}".format(chr(c), self.read_state, ord("\n"), c))
            if self.read_state == READ_STATE_TEXT:
                if c == CMD_IAC:
                    self.read_state = READ_STATE_CMD
                elif c == ord("\n"):
                    #self.text_buffer.append(c)
                    #HACK just decoding the data by default is not wise
                    try:
                        message = self.text_buffer.decode(self.encoding)
                        self.__push_message(message)
                    except Exception as e:
                        # HACK
                        print("{} - {}".format(self.text_buffer, e))
                        self.alive = False
                    #print("ding!")
                    self.text_buffer = bytearray()
                elif c == ord("\r"):
                    #print("ding!")
                    #self.text_buffer.append(c)
                    pass
                elif c == ord("\x08"):
                    self.text_buffer = self.text_buffer[:-1]
                else:
                    self.text_buffer.append(c)

            elif self.read_state == READ_STATE_CMD:
                if c == CMD_SB:
                    #self.se_buffer.append(c)
                    self.read_state = READ_STATE_SNEG
                elif c in (CMD_WILL, CMD_WONT, CMD_DO, CMD_DONT):
                    self.cmd_buffer.append(c)
                else:
                    self.cmd_buffer.append(c)
                    self.__push_command(self.cmd_buffer)
                    self.cmd_buffer = bytearray()
                    self.read_state = READ_STATE_TEXT

            elif self.read_state == READ_STATE_SNEG:
                self.se_buffer.append(c)
                if c == CMD_SE:
                    self.__push_subnegotiation(self.se_buffer)
                    self.se_buffer = bytearray()
                    self.read_state = READ_STATE_TEXT

        #if message:
        #    return message

    def send_string(self, s):
        self.sock.sendall(bytearray(s+"\n\r", self.encoding))


    def receive(self):
        rfiles, wfiles, xfiles = select.select([self.sock], [], [], 0)
        if rfiles:
        #while rfiles:
            try:
                data = self.sock.recv(self._tsize)
                if data:
                    #tmp_msg = self.parse(data)
                    self.parse(data)
                    #print(">>>{}<<<".format(tmp_msg))
                    #if tmp_msg:
                    #    self.__push_message(tmp_msg)
                else:
                    self.alive = False

            except socket.error as e:
                self.alive = False
            #rfiles, wfiles, xfiles = select.select([self.sock], [], [], 0)
