#from enum import Enum

import nxmudlib.util


iota = nxmudlib.util.make_iota()

STATE_FLOATING = iota()
STATE_WAIT_NAME = iota()
STATE_WAIT_PASSWORD = iota()
STATE_WAIT_ROLE = iota()

STATE_PLAYER = iota()
STATE_SPECTATOR = iota()
STATE_MACHINE = iota()

STATE_DEAD = iota()

class Player():
    def __init__(self):
        self.curstate = STATE_FLOATING
        self.name = None
        self.room = None
