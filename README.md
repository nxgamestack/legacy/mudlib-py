nxmudlib
===============================

version number: 0.0.1
author: nexustix

Overview
--------

Python library for creating MUDs

Installation / Usage
--------------------

To install use pip:

    $ git clone https://gitlab.com/nxgamestack/mudlib/mudlib-py.git
    $ pip install .
